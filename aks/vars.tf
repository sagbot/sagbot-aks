# Azure Login information
variable "azure_tenant_id" {
  description = "The id of the azure tenant to deploy in. defaults to values from az login command"
  default = null
  sensitive = true
}

variable "azure_subscription_id" {
  description = "The id of the azure subscription to deploy in. defaults to the subscription you logged in to using az login"
  default = null
  sensitive = true
}

variable "azure_auth_client_id" {
  description = "if set the client id and secret will be used to authenticate with azure"
  default = null
  sensitive = true
}

variable "azure_auth_client_secret" {
  description = "if set the client id and secret will be used to authenticate with azure"
  default = null
  sensitive = true
}

# General applicaiton config

variable "application_name" {
  default = "sagbot_aks"
}

variable "location" {
  description = "Location of the cluster."
  default = "West Europe"
}

variable "tags" {
  type = map(string)
  description = "Tags that are applied to every resource in the project"

  default = {
    Source = "terraform"
    Environment = "Dev"
    Application = "sagbot_aks"

    # Added this because my azure subscription keeps adding this tag to resources, and 
    # everytime I apply it detects a change and deletes this tag.
    CreationDateTime = "2020-12-10T22:00:22.9709038Z"
  }
}

# Detailed configuration

variable "virtual_network_address_prefix" {
  description = "VNET address prefix"
  default     = "15.0.0.0/8"
}

variable "aks_subnet_address_prefix" {
  description = "Subnet address prefix."
  default     = "15.0.0.0/16"
}

variable "app_gateway_subnet_address_prefix" {
  description = "Subnet server IP address."
  default     = "15.1.0.0/16"
}

variable "app_gateway_sku" {
  description = "Name of the Application Gateway SKU"
  default = "Standard_v2"
}

variable "aks_dns_prefix" {
  description = "Optional DNS prefix to use with hosted Kubernetes API server FQDN."
  default     = "sagbot"
}

variable "aks_agent_os_disk_size" {
  description = "Disk size (in GB) to provision for each of the agent pool nodes. This value ranges from 0 to 1023. Specifying 0 applies the default disk size for that agentVMSize."
  default     = 40
}

variable "aks_agent_count" {
  description = "The number of agent nodes for the cluster."
  default     = 3
}

variable "aks_agent_vm_size" {
  description = "VM size"
  default     = "Standard_D3_v2"
}

variable "aks_service_cidr" {
  description = "CIDR notation IP range from which to assign service cluster IPs"
  default     = "10.0.0.0/16"
}

variable "aks_dns_service_ip" {
  description = "DNS server IP address"
  default     = "10.0.0.10"
}

variable "aks_docker_bridge_cidr" {
  description = "CIDR notation IP for Docker bridge."
  default     = "172.17.0.1/16"
}