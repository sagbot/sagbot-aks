# Locals block for hardcoded names. 
locals {
    # In the future this will 
    name_prefix = "${var.application_name}_${terraform.workspace}"
}

resource "azurerm_resource_group" "rg" {
  name     = "${local.name_prefix}_rg"
  location = var.location

  tags = var.tags
}

# Virtual network for Aks
resource "azurerm_virtual_network" "vnet" {
  name                = "${local.name_prefix}_vnet"
  location            = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  address_space       = [var.virtual_network_address_prefix]

  tags = var.tags
}

resource "azurerm_subnet" "aks_subnet" {
  name                 = "${azurerm_virtual_network.vnet.name}_aks_subnet"
  resource_group_name = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.aks_subnet_address_prefix]
}

resource "azurerm_subnet" "appgw_subnet" {
  name                 = "${azurerm_virtual_network.vnet.name}_appgw_subnet"
  resource_group_name = azurerm_resource_group.rg.name
  virtual_network_name = azurerm_virtual_network.vnet.name
  address_prefixes     = [var.app_gateway_subnet_address_prefix]
}

# Public Ip 
resource "azurerm_public_ip" "pubip" {
  name = "${local.name_prefix}_pubip"
  location = azurerm_resource_group.rg.location
  resource_group_name = azurerm_resource_group.rg.name
  allocation_method = "Static"
  sku = "Standard"
  domain_name_label = var.aks_dns_prefix

  tags = var.tags
}

# Application gateway (AKS Ingress)
resource "azurerm_application_gateway" "appgw" {
  name                = "${local.name_prefix}_appgw"
  resource_group_name = azurerm_resource_group.rg.name
  location            = azurerm_resource_group.rg.location

  sku {
    name     = var.app_gateway_sku
    tier     = "Standard_v2"
    capacity = 2
  }

  gateway_ip_configuration {
    name      = "${local.name_prefix}_appgw_ip_config"
    subnet_id = azurerm_subnet.aks_subnet.id
  }

  frontend_port {
    name = "${local.name_prefix}_appgw_fe_port_name"
    port = 80
  }

  frontend_port {
    name = "httpsPort"
    port = 443
  }

  frontend_ip_configuration {
    name                 = "${local.name_prefix}_appgw_fe_ip_config"
    public_ip_address_id = azurerm_public_ip.pubip.id
  }

  backend_address_pool {
    name = "${local.name_prefix}_appgw_be_address_pool"
  }

  backend_http_settings {
    name                  = "${local.name_prefix}_appgw_be_http_settings"
    cookie_based_affinity = "Disabled"
    port                  = 80
    protocol              = "Http"
    request_timeout       = 1
  }

  http_listener {
    name                           = "${local.name_prefix}_appgw_http_listener"
    frontend_ip_configuration_name = "${local.name_prefix}_appgw_fe_ip_config"
    frontend_port_name             = "${local.name_prefix}_appgw_fe_port_name"
    protocol                       = "Http"
  }

  request_routing_rule {
    name                       = "${local.name_prefix}_appgw_routing_rule"
    rule_type                  = "Basic"
    http_listener_name         = "${local.name_prefix}_appgw_http_listener"
    backend_address_pool_name  = "${local.name_prefix}_appgw_be_address_pool"
    backend_http_settings_name = "${local.name_prefix}_appgw_be_http_settings"
  }

  tags = var.tags
}

# AKS
resource "azurerm_kubernetes_cluster" "aks" {
  name       = "${local.name_prefix}_aks"
  location   = azurerm_resource_group.rg.location
  dns_prefix = var.aks_dns_prefix

  resource_group_name = azurerm_resource_group.rg.name

  identity {
    type = "SystemAssigned"
  }

  default_node_pool {
    name            = "agentpool"
    node_count      = var.aks_agent_count
    vm_size         = var.aks_agent_vm_size
    os_disk_size_gb = var.aks_agent_os_disk_size
    vnet_subnet_id  = azurerm_subnet.appgw_subnet.id
  }

  network_profile {
    network_plugin     = "azure"
    dns_service_ip     = var.aks_dns_service_ip
    docker_bridge_cidr = var.aks_docker_bridge_cidr
    service_cidr       = var.aks_service_cidr
  }

  tags       = var.tags
}