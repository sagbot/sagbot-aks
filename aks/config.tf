terraform {
  required_version = "0.14.0"
  backend "http" {
    # This url has the gitlab project id hardcoded into it, unfortunately i can't use variables here
    address = "https://gitlab.com/api/v4/projects/26521776/terraform/state/tfstate"
    lock_address = "https://gitlab.com/api/v4/projects/26521776/terraform/state/tfstate/lock"
    unlock_address = "https://gitlab.com/api/v4/projects/26521776/terraform/state/tfstate/lock"
    lock_method = "POST"
    unlock_method = "DELETE"
  }

  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "2.39.0"
    }
  }
}

provider "azurerm" {
  client_id = var.azure_auth_client_id
  client_secret = var.azure_auth_client_secret
  subscription_id = var.azure_subscription_id
  tenant_id = var.azure_tenant_id

  features {}
}